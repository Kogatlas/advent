const { log, readInput, shuffleArray, varToString } = require('../../shared/index.js');

let safe = 0;
solve = (puzzleInput) => {
  puzzleInput.forEach(report => {
    const levels = report.split(' ');
    const increasing = levels[1] - levels[0] > 0;
    if (levels.slice(1).every((level, index) => {
      let diff = level - levels[index];
      return !!(Math.abs(diff) > 0 && Math.abs(diff) < 4 && diff > 0 === increasing);
    })) safe++;
  });
}

const input = readInput('input.txt');
shuffleArray(input);
solve(input);
log({ safe });