const { log, readInput, shuffleArray, varToString } = require('../../shared/index.js');

const isSafe = (levels) => {
  const increasing = levels[1] - levels[0] > 0;
  return levels.slice(1).some((level, index) => {
    let diff = level - levels[index];
    return !(Math.abs(diff) > 0 && Math.abs(diff) < 4 && diff > 0 === increasing);
  });
}

let safe = 0;
solve = (puzzleInput) => {
  puzzleInput.forEach(report => {
    const levels = report.split(' ');
    let unsafe = isSafe(levels);

    if (!unsafe) safe++
    else {
      for (let index = 0; index < levels.length; index++) {
        unsafe = isSafe(levels.slice(0, index).concat(levels.slice(index + 1)));
        if (!unsafe) {
          safe++;
          break;
        }
      }
    }
  });
}

const input = readInput('input.txt');
shuffleArray(input);
solve(input);
log({ safe });