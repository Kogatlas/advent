const { log, readInput, shuffleArray, varToString } = require('../../shared/index.js');

solve = (puzzleInput) => {
  let leftList = [], rightListOccurences = {};
  puzzleInput.forEach((locationId) => {
    const [left, right] = locationId.toString().split('   ');
    leftList.push(left);
    !!rightListOccurences[right] ? rightListOccurences[right]++ : rightListOccurences[right] = 1;
  });
  leftList.sort((a, b) => a - b);

  let similarityScore = 0;
  leftList.forEach(locationId => {
    similarityScore += locationId * rightListOccurences[locationId] || 0;
  });
  log({ similarityScore });
}

const input = readInput('input.txt');
shuffleArray(input);
solve(input);