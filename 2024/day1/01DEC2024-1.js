const { log, readInput, shuffleArray, varToString } = require('../../shared/index.js');

solve = (puzzleInput) => {
  let leftList = [], rightList = [];
  puzzleInput.forEach((locationId) => {
    const [left, right] = locationId.toString().split('   ');
    leftList.push(left);
    rightList.push(right);
  });
  leftList.sort((a, b) => a - b);
  rightList.sort((a, b) => a - b);

  let distance = 0;
  leftList.forEach((locationId, index) => {
    distance += Math.abs(locationId - rightList[index]);
  })
  log({ distance });
}

const input = readInput('input.txt');
shuffleArray(input);
solve(input);