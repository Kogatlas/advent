const { log, readInput, shuffleArray, varToString } = require('../../shared/index.js');

let sum = 0;
solve = (puzzleInput) => {
  const regexp = /mul\([0-9]+,[0-9]+\)/gu;
  puzzleInput.forEach(input => {
    const matches = [...input.matchAll(regexp)];
    sum += matches.reduce((acc, match) => {
      const [x, y] = match[0].matchAll(/\d+/g);
      return acc + x * y;
    }, 0);
  });
}

const input = readInput('input.txt');
shuffleArray(input);
solve(input);
log({ sum });