const { log, readInput, shuffleArray, varToString } = require('../../shared/index.js');

let sum = 0;
let enabled = true;
solve = (puzzleInput) => {
  const regexp = /mul\([0-9]+,[0-9]+\)|do\(\)|don't\(\)/gu;
  puzzleInput.forEach(input => {
    const matches = [...input.matchAll(regexp)];
    const roundTotal = matches.reduce((acc, match) => {
      if (match.toString().includes('do')) {
        enabled = !match.toString().includes('t');
        return acc;
      }
      if (!enabled) return acc;
      const [x, y] = match[0].matchAll(/\d+/g);
      return acc + x * y;
    }, 0);
    sum += roundTotal;
  });
}

const input = readInput('input.txt');
solve(input);
log({ sum });