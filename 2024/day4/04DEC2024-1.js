const { log, readInput, shuffleArray, varToString } = require('../../shared/index.js');

const directions = [
  { label: 'N', validation: (x, y, rows, cols) => y > 2, steps: { x: 0, y: -1 } },
  { label: 'NE', validation: (x, y, rows, cols) => y > 2 && x < cols - 3, steps: { x: 1, y: -1 } },
  { label: 'E', validation: (x, y, rows, cols) => x < cols - 3, steps: { x: 1, y: 0 } },
  { label: 'SE', validation: (x, y, rows, cols) => y < rows - 3 && x < cols - 3, steps: { x: 1, y: 1 } },
  { label: 'S', validation: (x, y, rows, cols) => y < rows - 3, steps: { x: 0, y: 1 } },
  { label: 'SW', validation: (x, y, rows, cols) => y < rows - 3 && x > 2, steps: { x: -1, y: 1 } },
  { label: 'W', validation: (x, y, rows, cols) => x > 2, steps: { x: -1, y: 0 } },
  { label: 'NW', validation: (x, y, rows, cols) => y > 2 && x > 2, steps: { x: -1, y: -1 } },
]

const letters = 'XMAS'.split('');

const check = (dir, col, row, rows) => {
  const { x, y } = dir;
  return letters.every(letter => {
    const result = rows[row].split('')[col];
    row += y;
    col += x;
    return letter === result;
  });
}

let matches = 0;
solve = (rows) => {
  rows.forEach((row, yIndex) => {
    row.split('').forEach((col, xIndex) => {
      if (col === letters[0]) {
        directions.forEach(direction => {
          const { label, validation, steps } = direction;
          if (validation(xIndex, yIndex, rows.length, row.length)) {
            matches += +check(steps, xIndex, yIndex, rows);
          }
        });
      }
    });
  });
}

const input = readInput('input.txt');
solve(input);
log({ matches });