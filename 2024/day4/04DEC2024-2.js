const { log, readInput, shuffleArray, varToString } = require('../../shared/index.js');

const directions = [
  { label: 'N', validation: (x, y, rows, cols) => y > 1, steps: { x: 1, y: -1 } },
  { label: 'E', validation: (x, y, rows, cols) => x < cols - 2, steps: { x: 1, y: 1 } },
  { label: 'S', validation: (x, y, rows, cols) => y < rows - 2, steps: { x: 1, y: 1 } },
  { label: 'W', validation: (x, y, rows, cols) => x > 1, steps: { x: -1, y: 1 } },
]

const letters = 'MAS'.split('');

const check = (dir, col, row, rows, label) => {
  const { x, y } = dir;
  const origRow = row;
  const origCol = col;
  return letters.every((letter, index) => {
    const result = rows[row].split('')[col];
    if (['E', 'W'].includes(label)) {
      const pair = rows[origRow + 2 - index]?.split('')[col];
      if (pair !== letter) return false;
    }
    if (['N', 'S'].includes(label)) {
      const pair = rows[row]?.split('')[origCol + 2 - index];
      if (pair !== letter) return false;
    }
    row += y;
    col += x;
    return letter === result;
  });
}

let matches = 0;
solve = (rows) => {
  rows.forEach((row, yIndex) => {
    row.split('').forEach((col, xIndex) => {
      if (col === letters[0]) {
        directions.forEach(direction => {
          const { label, validation, steps } = direction;
          if (validation(xIndex, yIndex, rows.length, row.length)) {
            matches += +check(steps, xIndex, yIndex, rows, label);
          }
        });
      }
    });
  });
}

const input = readInput('input.txt');
solve(input);
log({ matches });