const fs = require('fs');
const { varToString, log } = require('../../shared/log');

readInput = (file) => {
  var input = fs.readFileSync(file, 'utf-8');
  return input.split('\n\n');
}

shuffleArray = (array) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

unique = (value, index, self) => {
  return self.indexOf(value) === index;
}

let sum = 0;
solve = (puzzleInput) => {
  const groups = puzzleInput.map(line => line.split('\n'));
  log({groups});
  groups.forEach(group => {
    const uniqueItems = group.join('').split('').filter(unique);
    log({uniqueItems});
    log({group});
    uniqueItems.forEach(item => {
      if (group.every(person => person.split('').includes(item)))
        sum++;
    })
  });
}

const input = readInput('input.txt');
// const input = readInput('testInput.txt');
log({input});
shuffleArray(input);
solve(input);
log({sum});