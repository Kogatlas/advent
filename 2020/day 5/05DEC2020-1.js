const fs = require('fs');
const { varToString, log } = require('../../shared/log');

readInput = (file) => {
  var input = fs.readFileSync(file, 'utf-8');
  return input.split('\n');
}

shuffleArray = (array) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

// pipe = (...fns) => (x) => fns.reduce((v, f) => f(v), x);
let max = -Infinity;
const numberOfRows = 128;
const numberOfSeats = 8;
solve = (puzzleInput) => {
  log({puzzleInput});
  let rows = [];
  puzzleInput.forEach(boardingPass => {
    let rowsArray = [...Array(numberOfRows).keys()];
    let index = 0;
    // log({rowsLeft: rowsArray.length});
    while (rowsArray.length > 1) {
      const half = rowsArray.length / 2;
      rowsArray.splice(boardingPass[index++] === 'F' ? half : 0, half);
      // log({rowsLeft: rowsArray.length});
    }
    rows.push(rowsArray[0]);
    // log({RowNumber: rowsArray});
  });
  // log({rows});

  let seats = [];
  puzzleInput.forEach(boardingPass =>  {
    boardingPass = boardingPass.split('').splice(7);
    // log({boardingPass});
    let seatsArray = [...Array(numberOfSeats).keys()];
    let index = 0;

    while (seatsArray.length > 1) {
      const half = seatsArray.length / 2;
      seatsArray.splice(boardingPass[index++] === 'L' ? half : 0, half);
    }

    seats.push(seatsArray[0]);
  });
  // log({seats});
  for(let i = 0; i < puzzleInput.length; i++) {
    const id = (rows[i] * 8) + seats[i];
    max = max > id ? max : id;
    // log({result: (rows[i] * 8) + seats[i]});
  }
}

// const input = readInput('testInput.txt');
const input = readInput('input.txt');
// shuffleArray(input);
solve(input);
log({max});