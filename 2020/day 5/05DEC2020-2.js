const fs = require('fs');
const { varToString, log } = require('../../shared/log');

readInput = (file) => {
  var input = fs.readFileSync(file, 'utf-8');
  return input.split('\n');
}

shuffleArray = (array) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

// pipe = (...fns) => (x) => fns.reduce((v, f) => f(v), x);
let expected = [];
let ids = [];
let max = -Infinity;
const numberOfRows = 128;
const numberOfSeats = 8;
expectedSeats = (arr) => {
  [...Array(numberOfRows).keys()].forEach(row => {
    [...Array(numberOfSeats).keys()].forEach(seat => {
      arr.push((row + 1) * 8 + (seat + 1));
    });
  });
}

solve = (puzzleInput) => {
  // log({numberOfPasses: puzzleInput.length});
  expectedSeats(expected); // 8 * 128 = 1024
  log({expected});
  log({expected: expected.length});

  let rows = [];
  puzzleInput.forEach(boardingPass => {
    let rowsArray = [...Array(numberOfRows).keys()];
    let index = 0;
    while (rowsArray.length > 1) {
      const half = rowsArray.length / 2;
      rowsArray.splice(boardingPass[index++] === 'F' ? half : 0, half);
    }
    rows.push(rowsArray[0]);
  });

  let seats = [];
  puzzleInput.forEach(boardingPass =>  {
    boardingPass = boardingPass.split('').splice(7);
    let seatsArray = [...Array(numberOfSeats).keys()];
    let index = 0;

    while (seatsArray.length > 1) {
      const half = seatsArray.length / 2;
      seatsArray.splice(boardingPass[index++] === 'L' ? half : 0, half);
    }

    seats.push(seatsArray[0]);
  });

  for(let i = 0; i < puzzleInput.length; i++) {
    const id = (rows[i] * 8) + seats[i];
    ids.push(id);
    max = max > id ? max : id;
  }

  expected.forEach(possibleID => {
    if (!ids.includes(possibleID)) console.log(possibleID);
  });
}

// const input = readInput('testInput.txt');
const input = readInput('input.txt');
// shuffleArray(input);
solve(input);
// ids.sort((a, b) => a - b).forEach((id, index) => {
//   const before = ids[index - 1];
//   const after = ids[index + 1];
//   if (!before || !after) {
//     console.log(ids[index]);
//     console.log('');
//   }
// });
log({Total: ids.length});
log({max});