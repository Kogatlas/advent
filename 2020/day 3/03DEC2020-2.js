const fs = require('fs');

varToString = varObj => Object.keys(varObj)[0];
log = (...values) => {
  let msg = '';
  values.forEach(value => {
    const name = varToString(value);
    if (msg.length > 0) msg += ' ';
    msg += name.toUpperCase() + ':';
    msg += value[name];
  });
  console.log(msg);
}

readInput = (file) => {
  var input = fs.readFileSync(file, 'utf-8');
  return input.split('\n');
}

shuffleArray = (array) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

solve = (mapSegment, slope) => {
  const [x, y] = slope;
  const bottom = mapSegment.length;
  const segmentWidth = mapSegment[0].length;
  let [currX, currY] = [1, 1];

  do {
    currY += y;
    currX += x;
    var next = mapSegment[currY - 1][(currX - 1) % segmentWidth];
    if (next === tree) {
      trees++;
      // log({msg:'tree found'})
    }
    // log({next}, {currX}, {currY});
  } while (currY < bottom);
}

const input = readInput('input.txt');
// shuffleArray(input);
// Right 1, down 1.
// Right 3, down 1. (This is the slope you already checked.) 178
// Right 5, down 1.
// Right 7, down 1.
// Right 1, down 2.
const slopes = [
  [1, 1],
  [3, 1],
  [5, 1],
  [7, 1],
  [1, 2],
];
const clear = '.';
const tree = '#';
let trees = 0;
slopes.forEach((slope, index) => {
  solve(input, slope);
  log({trees});
  slopes[index] = trees;
  trees = 0
});

console.log(slopes.reduce((a, b) => a * b));
