const fs = require('fs');

varToString = varObj => Object.keys(varObj)[0];
log = (...values) => {
  let msg = '';
  values.forEach(value => {
    const name = varToString(value);
    if (msg.length > 0) msg += ' ';
    msg += name.toUpperCase() + ':';
    msg += value[name];
  });
  console.log(msg);
}

readInput = (file) => {
  var input = fs.readFileSync(file, 'utf-8');
  return input.split('\n');
}

shuffleArray = (array) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

const clear = '.';
const tree = '#';
let trees = 0;
solve = (mapSegment) => {
  const bottom = mapSegment.length;
  const segmentWidth = mapSegment[0].length;
  let [currX, currY] = [1, 1];

  do {
    currY++;
    currX += 3;
    var next = mapSegment[currY - 1][(currX - 1) % segmentWidth];
    if (next === tree) {
      trees++;
      log({msg:'tree found'})
    }
    log({next}, {currX}, {currY});
  } while (currY < bottom);
}

const input = readInput('input.txt');
// shuffleArray(input);
solve(input);
log({trees});
