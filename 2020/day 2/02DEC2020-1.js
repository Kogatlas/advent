const fs = require('fs');

const readInput = (file) => {
  var input = fs.readFileSync(file, 'utf-8');
  return input.split('\n');
}

const shuffleArray = (array) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

let invalidCount = 0;
const solve = (puzzleInput) => {
  puzzleInput.forEach((value) => {
    let [ range, token, password ] = value.split(' ');
    const [ min, max ] = range.split('-').filter(Number);
    let count = password.split('').filter(c => token.includes(c)).length;
    if (count < min || count > max) {
      invalidCount++;
    }
  });
}

var input = readInput('input.txt');
shuffleArray(input);
solve(input);
console.log(invalidCount);