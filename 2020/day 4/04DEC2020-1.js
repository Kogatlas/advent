const fs = require('fs');
const { varToString, log } = require('../../shared/log');

readInput = (file) => {
  var input = fs.readFileSync(file, 'utf-8');
  return input.split('\n\n');
}

shuffleArray = (array) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

let invalid = 0;
let valid = 0;
solve = (puzzleInput) => {
  const passports = puzzleInput.map(line => line.split('\n').join(' ').split(' '));
  passports.forEach(passport => {
    let fields = {};
    log({passport});
    passport.map(field => {
      const [key, value] = field.split(':');
      fields[key] = value;
    });
    log({fields});

    validate(fields) ? valid++ : invalid++;
  });
}

validate = (fields) => {
  return Object.keys(fields).length === 8 || (Object.keys(fields).length === 7 && !fields.cid);
}
// byr (Birth Year)
// iyr (Issue Year)
// eyr (Expiration Year)
// hgt (Height)
// hcl (Hair Color)
// ecl (Eye Color)
// pid (Passport ID)
// cid (Country ID)
const input = readInput('input.txt');
// log({input});
// log({input: input.map(line => line.split('\n').join(' '))});

// shuffleArray(input);
solve(input);
log({invalid}, {valid});