const fs = require('fs');
const { varToString, log } = require('../../shared/log');

readInput = (file) => {
  var input = fs.readFileSync(file, 'utf-8');
  return input.split('\n\n');
}

shuffleArray = (array) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

let invalid = 0;
let valid = 0;
solve = (puzzleInput) => {
  const passports = puzzleInput.map(line => line.split('\n').join(' ').split(' '));
  passports.forEach(passport => {
    let fields = {};
    passport.map(field => {
      const [key, value] = field.split(':');
      fields[key] = value;
    });

    validate(fields) ? valid++ : invalid++;
  });
}

validate = (fields) => {
  if (Object.keys(fields).length !== 8) {

    if (!Object.keys(fields).includes('cid') && Object.keys(fields).length === 7) {
    } else {
      // log({fields});
      return false;
    }
  }
  // if (isNaN(fields[byr]) || fields[byr].length !== 4 || fields[byr] < 1920 || fields[byr] > 2002) return false;
  // if (isNaN(fields[iyr]) || fields[iyr].length !== 4 || fields[iyr] < 2010 || fields[iyr] > 2020) return false;
  // if (isNaN(fields[eyr]) || fields[eyr].length !== 4 || fields[eyr] < 2020 || fields[eyr] > 2030) return false;
  let valid = true;
  Object.keys(fields).forEach(field => {
    switch(field) {
      case 'byr': // byr (Birth Year) - four digits; at least 1920 and at most 2002.
        if (isNaN(fields[field]) || fields[field].length !== 4 || fields[field] < 1920 || fields[field] > 2002) {
          valid = false;
          // log({field: fields[field]});
        }
        break;
      case 'iyr': // iyr (Issue Year) - four digits; at least 2010 and at most 2020.
        if (isNaN(fields[field]) || fields[field].length !== 4 || fields[field] < 2010 || fields[field] > 2020) {
          valid = false;
          // log({field: fields[field]});
        }
        break;
      case 'eyr': // eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
        if (isNaN(fields[field]) || fields[field].length !== 4 || fields[field] < 2020 || fields[field] > 2030) {
          valid = false;
          // log({field: fields[field]});
        }
        break;
      case 'hgt': // hgt (Height) - a number followed by either cm or in:
        var number = parseInt(fields[field]);
        var unit = fields[field].split('').filter(isNaN).join('');
        if (!number || !unit) valid = false;
        // valid = valid && (number && unit);

        if (unit === 'cm') { // If cm, the number must be at least 150 and at most 193.
          if (number < 150 || number > 193)
            valid = false;
        } else if (unit === 'in') { // If in, the number must be at least 59 and at most 76.
          if (number < 59 || number > 76)
            valid = false;
        } else {
          valid = false;
        }
        break;
      case 'hcl': // hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
        const hcl = fields[field];
        if (hcl.split('')[0] !== '#' || hcl.length !== 7 || !hcl.match(/(^#[0-9a-fA-F]{6})$/)) {
          valid = false;
          // log({hcl});
        }
        // valid = (hcl.split('')[0] === '#' && hcl.length === 7 && fields[field].toString().match('0[xX][0-9a-fA-F]+'));
        break;
      case 'ecl': // ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
        // valid = valid && ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'].includes(fields[field]);
        if (!['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'].includes(fields[field])) {
          // log({'ecl': fields[field]});
          valid = false;
        }
        break;
      case 'pid': // pid (Passport ID) - a nine-digit number, including leading zeroes.
        // log({field: fields[field]});
        const pid = fields[field];
        if (pid.length !== 9) {
          valid = false;
          // log({pid});
        }
        // valid = valid && pid.length === 9;
        break;
      case 'cid': // cid (Country ID) - ignored, missing or not.
        // log({field: fields[field]});
        break;
    }
  });
  // log({valid});
  return valid;
}

// const input = readInput('testInput.txt');
const input = readInput('input.txt');

// log({input: input.map(line => line.split('\n').join(' '))});
// shuffleArray(input);
solve(input);
log({invalid}, {valid});
// 224
// 220 - 233 250