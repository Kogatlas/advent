const fs = require('fs');
const { varToString, log } = require('../../shared/log');

readInput = (file) => {
  var input = fs.readFileSync(file, 'utf-8');
  return input.split('\n');
}

shuffleArray = (array) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

unique = (value, index, self) => {
  return self.indexOf(value) === index;
}

const myBag = 'shiny gold';
const rules = [];
let colors =  [];
let possibilities = [];
const canContain = (rule) => {
  // log({rule});
  if (colors.includes(myBag)) {
    // console.log('found it', rule);
    return true;
  } else {
    if (rule.contents.length > 0) {
      rule.contents.forEach(possible => {log
        colors.push(possible.color);
        canContain(rules.find(r => r.color === possible.color));
      });
    }
  }
}

solve = (puzzleInput) => {
  puzzleInput.forEach(line => {
    const segments = line.split(' ');

    let rule = {
      color: segments[0] + ' ' + segments[1],
      contents: [],
    };

    let remaining = [...segments.slice(4)];
    if (!isNaN(remaining[0])) {
      for (let bags = 1; bags <= remaining.length / 4;) {
        rule.contents.push({
          amount: remaining[0],
          color: remaining[1] + ' ' + remaining[2],
        });
        remaining = remaining.slice(4);
      }
    }
    rules.push(rule);

  });

  rules.forEach(rule => {
    // log({result: canContain(rule)});
    canContain(rule);
    // log({colors});
    if (colors.includes(myBag)) possibilities.push(rule.color);
    colors = [];
    // if (canContain(rule)) log({rule});
  });
}

const input = readInput('input.txt');
// const input = readInput('testInput.txt');
// shuffleArray(input);
solve(input);
// log({possibilities});
log({total: possibilities.length});
// log({total: possibilities.filter(unique).length});
// rules.forEach(rule => log({rule}));
// log({rules: rules.length});