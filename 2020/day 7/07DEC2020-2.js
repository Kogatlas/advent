const fs = require('fs');
const { varToString, log } = require('../../shared/log');

readInput = (file) => {
  var input = fs.readFileSync(file, 'utf-8');
  return input.split('\n');
}

shuffleArray = (array) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

// unique = (value, index, self) => {
//   return self.indexOf(value) === index;
// }

const myBag = 'shiny gold';
const rules = [];
parseRules = (input) => {
  input.forEach(line => {
    const segments = line.split(' ');

    let rule = {
      color: segments[0] + ' ' + segments[1],
      contents: [],
    };

    let remaining = [...segments.slice(4)];
    if (!isNaN(remaining[0])) {
      for (let bags = 1; bags <= remaining.length / 4;) {
        rule.contents.push({
          amount: remaining[0],
          color: remaining[1] + ' ' + remaining[2],
        });
        remaining = remaining.slice(4);
      }
    }
    rules.push(rule);

  });
};

getBag = (color) => {
  return rules.find(r => r.color === color);
}

let bags = [];
solve = (input) => {
  // log({input});

  input.contents.forEach(bag => {
    for (let i = 0; i < bag.amount; i++) {
      bags.push(bag.color);
      solve(getBag(bag.color));
    }
  });
}

const input = readInput('input.txt');
// const input = readInput('testInput.txt');
// shuffleArray(input);
parseRules(input);
solve(getBag(myBag), 1);
// log({rules: rules.length});
// rules.forEach(rule => log({rule}));
log({bags});
log({count: bags.length});