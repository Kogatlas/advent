const fs = require('fs');

const readInput = (file) => {
  var input = fs.readFileSync(file, 'utf-8');
  return input.split('\n');
}

const shuffleArray = (array) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

var input = readInput('input.txt');
shuffleArray(input);

var result;
var computations = 0;
var compute = (values) => {
  for(let first in values) { // values.some((first) => {
    if (result) break;
    // console.log('first');
    var newValues = [...values].splice(1, values.length);
    var thirdDimension = [...newValues].splice(1, newValues.length);

    for(let second in newValues) { // newValues.some((second) => {
      if (result) break;
      // console.log('second');

      for(let third in thirdDimension) { // thirdDimension.some((third) => {
        // computations++;
        // console.log('third');

        // console.log('first:', first);
        // console.log('second:', second);
        // console.log('third:', third);
        // console.log('total:', first + second + third);
        var thousandPlus = [first, second, third].filter(item => (item / 1000) >= 1).length;

        if (thousandPlus > 1) {
          break;
        } else {
          computations++;
          if (first + second + third === 2020) {
            result = first * second * third;
            console.log('Found it!', result);
            break;
          }
        };
      };

      if (result) {
        // console.log('DING DING DING', result);
        break;
      } else {
        setTimeout(() => {
          if (!result)
            compute(newValues);
        }, 0);
      } // third
    }; // second
  }; // first
}

compute(input);
console.log('Total computations:', computations, 'out of 7,880,400');
