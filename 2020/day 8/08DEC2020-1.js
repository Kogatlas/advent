const fs = require('fs');
const { varToString, log } = require('../../shared/log');

readInput = (file) => {
  var input = fs.readFileSync(file, 'utf-8');
  return input.split('\n');
}

shuffleArray = (array) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

solve = (instructions) => {
  log({instructions});
  let accumulator = 0;
  let executed = [];
  let index = 0;
  while (true) {
    log({instruction: instructions[index]});
    if (executed.includes(index)) {
      log({found: accumulator});
      return accumulator;
    } else {
      executed.push(index);
      log({executed});
      const [operation, argument] = instructions[index].split(' ');
      // log({operation});
      // log({argument});
      switch (operation) {
        case 'jmp':
          index += parseInt(argument);
          log({jmp: index});
          break;
        case 'acc':
          accumulator += parseInt(argument);
          log({acc: accumulator});
        case 'nop':
        default:
          index++;
      }
    }
  }
}

const input = readInput('input.txt');
// const input = readInput('testInput.txt');
// shuffleArray(input);
const answer = solve(input);
log({answer});