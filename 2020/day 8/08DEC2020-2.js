const fs = require('fs');
const { varToString, log } = require('../../shared/log');

readInput = (file) => {
  var input = fs.readFileSync(file, 'utf-8');
  return input.split('\n');
}

shuffleArray = (array) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

// is = (command, type) => command.includes(type);
flipCmd = (cmd) => cmd === 'nop' ? 'jmp' : 'nop';

solve = (instructions) => {
  log({instructions});
  let executed = [];
  let notBugged = [];
  let accumulator = 0;
  let index = 0;
  let testingIndex = instructions.findIndex(i => !notBugged.includes(i) && (i.includes('nop') || i.includes('jmp')));
  log({testingIndex});
  // instructions[testingIndex] = instructions[testingIndex].includes('nop')

  while (testingIndex >= 0) {
    if (executed.includes(index)) {
      executed = [];
      notBugged.push(testingIndex);
      // log({notBugged});
      accumulator = 0;
      index = 0;
      testingIndex = instructions.findIndex((cmd, i) => !notBugged.includes(i) && (cmd.includes('nop') || cmd.includes('jmp')));
      // log({testingIndex});
    } else {
      if (index !== instructions.length) {
        log({instruction: instructions[index]});
        executed.push(index);
        log({executed});
        let [operation, argument] = instructions[index].split(' ');
        if (index === testingIndex)
          operation = flipCmd(operation);
        // log({operation});
        // log({argument});
        switch (operation) {
          case 'jmp':
            index += parseInt(argument);
            log({jmp: index});
            break;
          case 'acc':
            accumulator += parseInt(argument);
            log({acc: accumulator});
          case 'nop':
          default:
            index++;
        }
      } else {
        // log({found: accumulator});
        return accumulator;
      }
    }
  }
}

const input = readInput('input.txt');
// const input = readInput('testInput.txt');
// shuffleArray(input);
const answer = solve(input);
log({answer});