const { log, readInput } = require('../../shared/index.js');

const deviceCmds = {
  addx: (x) => {
    cycle();
    cycle();
    register += +x;
  },
  noop: () => cycle(),
};

const cycle = () => {
  const row = Math.floor((currentCycle - 1)/ 40);
  const column = (currentCycle - 1) % 40;
  screen[row][column] = [register - 1, register, register + 1].includes(column) ? '#' : '.';
  currentCycle++;
};

let register = currentCycle = 1;
let screen = [];
solve = (instructions) => {
  for (let i = 0; i < 6; i++) {
    screen.push(Array.from({length: 40}, () => '.'));
  };

  
  instructions.forEach(instruction => {
    const [cmd, arg] = instruction.split(' ');
    deviceCmds[`${cmd}`](arg);
  });
  print();
};

const print = () => {
  screen.forEach(row => console.log(row.toString().replaceAll(',','')));
};

// const input = readInput('practice.txt');
const input = readInput('input.txt');
solve(input);

// 40 x 6
// ##..##..##..##..##..##..##..##..##..##..
// ###...###...###...###...###...###...###.
// ####....####....####....####....####....
// #####.....#####.....#####.....#####.....
// ######......######......######......####
// #######.......#######.......#######.....