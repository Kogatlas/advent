const { log, readInput } = require('../../shared/index.js');

const deviceCmds = {
  addx: (x) => {
    cycle();
    register += +x;
    cycle();
  },
  noop: () => cycle(),
};

const cycle = () => {
  currentCycle++;
  if (!((currentCycle - 20) % 40)) {
    signal += register * currentCycle;
  };
};

let register = currentCycle = 1;
let signal = 0;
solve = (puzzleInput) => {
  puzzleInput.forEach(instructions => {
    const [cmd, arg] = instructions.split(' ');
    deviceCmds[`${cmd}`](arg);
  });
  log({signal});
};

// const input = readInput('practice.txt');
const input = readInput('input.txt');
solve(input);