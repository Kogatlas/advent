const { log, readInput } = require('../../shared/index.js');

let total = 0;
calculate = (match) => {
  if (!!match && match === match.toUpperCase()) {
    total += match.charCodeAt(0) - 38;
  } else if (!!match) {
    total += match.charCodeAt(0) - 96;
  }

  return total;
}

solve = (rucksacks) => {
  for (let index = 0; index < rucksacks.length; index += 3) {
    const group = rucksacks.slice(index, index + 3);

    let overlap = [...group[0]].filter(ind => group[1]?.includes(ind));
    let match = new Set(overlap.filter(ind => group[2]?.includes(ind)));// overlap =
    
    calculate([...match][0])
  }
}

const input = readInput('input.txt');
solve(input);
log({total});