const { log, readInput, shuffleArray } = require('../../shared/index.js');

solve = (rucksacks) => {
  let total = 0;

  rucksacks.map(rucksack => {
    const [one, two] = [
      [rucksack.slice(0, rucksack.length / 2)],
      [rucksack.slice(rucksack.length / 2, rucksack.length)],
    ];
    
    const mistake = Array.from(one.toString()).find(supply => {
      return Array.from(two.toString()).includes(supply);
    });
  
    if (!!mistake && mistake === mistake.toUpperCase()) {
      total += mistake.charCodeAt(0) - 38;
    } else if (!!mistake) {
      total += mistake.charCodeAt(0) - 96;
    }
  });
  
  log({total});
}

const input = readInput('input.txt');
shuffleArray(input);
solve(input);