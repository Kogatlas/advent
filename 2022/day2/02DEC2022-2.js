const { log, readInput } = require('../../shared/index.js');

const handscore = {
	'rock' : 1,
  'paper' : 2,
  'scissors' : 3,
};

const outcomeScore = {
	'lose' : 0, // 0
  'draw' : 3, // 1
  'win' : 6, // 2
};

const translateThrow = (hand) => {
  switch (hand) {
    case 'A':
      return 'rock';
    case 'B':
      return 'paper';
    case 'C':
      return 'scissors'
  }
};

const translateOutcome = (outcome) => {
  switch (outcome) {
    case 'X':
      return 'lose';
    case 'Y':
      return 'draw';
    case 'Z':
      return 'win'
  }
};

const calculate = (outcome, oppHandScore) => {
  console.log(outcome, oppHandScore);
  switch (outcome) {
    case 'draw':
      return oppHandScore;
    case 'win':
      return 1 + oppHandScore % 3;
    case 'lose':
      return 1 + (oppHandScore + 1) % 3;
  }
};

solve = (puzzleInput) => {
  let score = 0;
  puzzleInput.map(round => {
    const [ opp, outcome ] = round.split(' ');
    const oppThrow = translateThrow(opp);
  
    const desiredOutcome = translateOutcome(outcome);
  
    const result = calculate(desiredOutcome, handscore[oppThrow]);
    score += outcomeScore[desiredOutcome] + result;
    log({score});
  });
}

const input = readInput('input.txt');
solve(input);