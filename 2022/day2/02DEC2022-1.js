const { log, readInput, shuffleArray } = require('../../shared/index.js');

const handscore = {
	'rock' : 1,
  'paper' : 2,
  'scissors' : 3,
};

const translateThrow = (hand) => {
  switch (hand) {
    case 'A':
    case 'X':
      return 'rock';
    case 'B':
    case 'Y':
      return 'paper';
    case 'C':
    case 'Z':
      return 'scissors'
  }
};

const outcome = (p1, p2) => {
  switch (p1 - p2) {
    case -2:
    case 1:
      return 6;
    case 2:
    case -1:
      return 0;
    case 0:
      return 3;
  }
};

// A = Rock; B = Paper; C = Scissors
// X = Rock; Y = Paper; Z = Scissors
// Rock = 1; Paper = 2; Scissors = 3;
// Lose = 0; Draw = 3; Win = 6;
solve = (puzzleInput) => {
  let score = 0;

  puzzleInput.map(round => {
    const [ opp, me ] = round.split(' ');
    const oppThrow = handscore[translateThrow(opp)];
    const myThrow = handscore[translateThrow(me)];
  
    const oc = outcome(myThrow, oppThrow) + myThrow;

    score += oc;
    log({score});
  });
}

const input = readInput('input.txt');
solve(input);