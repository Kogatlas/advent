const { log, readInput } = require('../../shared/index.js');

let x = 11;
let y = 5;
let traversed = [`${x},${y}`];
let rope = Array.from({length: 10}, () => [x, y]);

solve = (puzzleInput) => {
  puzzleInput.forEach(move => {
    shift(move);
  });

  log({count: new Set(traversed).size});
};

const range = (knot, prevKnot, coord) => Math.abs(knot[coord] - prevKnot[coord]);

const shift = (move) => {
  const [direction, distance] = move.split(' ');
  for (let step = 1; step <= distance; step++) {
    rope.map((knot, index) => {
      const prevKnot = rope[index -1] || knot;
      if (!index) {
        switch (direction) {
          case 'R':
            ++knot[0];
            break;
          case 'D':
            --knot[1];
            break;
          case 'L':
            --knot[0];    
            break;
          case 'U':
            ++knot[1];            
            break;
        };
      };

      if (range(knot, prevKnot, 1) > 1) {
        knot[1] += prevKnot[1] - knot[1] > 0 ? 1 : -1;

        if (range(knot, prevKnot, 0) > 0)
          knot[0] += prevKnot[0] - knot[0] > 0 ? 1 : -1;
      };

      if (range(knot, prevKnot, 0) > 1) {
        knot[0] += prevKnot[0] - knot[0] > 0 ? 1 : -1;

        if (range(knot, prevKnot, 1) > 0)
          knot[1] += prevKnot[1] - knot[1] > 0 ? 1 : -1;
      };

      if (index === 9) {
        const position = `${knot[0]},${knot[1]}`;
        traversed.push(position);
      };
    });
  };
  // print();
};

let prevState = [];
const print = () => {
  if (rope.every((knot, index) => knot.toString() == prevState[index]?.toString())) {
    console.log('no change');
    return;
  } else {
    for (let i = 20; i >= 0; i--) {
      let line = '';
      for (let j = 0; j <= 25; j++) {
        let found = rope.findIndex(knot => {
          return knot.toString() == [ j, i ];
        });
        if (found >= 0) {
          line += found || 'H';
        } else {
          if (j === x && i === y) {
            line += 's';
          } else line += '.';
        };
      };
      console.log(line);
    };
    prevState = JSON.parse(JSON.stringify(rope));
    console.log();
    console.log('');
    console.log();
  };
};

// const input = readInput('input.txt');
const input = readInput('practice.txt');
solve(input);

// == Initial State ==
// 26 x 21
// ..........................
// ..........................
// ..........................
// ..........................
// ..........................
// ..........................
// ..........................
// ..........................
// ..........................
// ..........................
// ..........................
// ..........................
// ..........................
// ..........................
// ..........................
// ...........H..............  (H covers 1, 2, 3, 4, 5, 6, 7, 8, 9, s)
// ..........................
// ..........................
// ..........................
// ..........................
// ..........................