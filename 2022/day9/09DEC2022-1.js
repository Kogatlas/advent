const { log, readInput } = require('../../shared/index.js');

let x = 0;
let y = 0;
let traversed = [`${x},${y}`];
let head = { x, y };
let tail = { x, y };

solve = (puzzleInput) => {
  puzzleInput.forEach(move => {
    shift(move);
  });

  log({results: new Set(traversed)});
  log({count: new Set(traversed).size});
};

const outOfRange = (coord) => Math.abs(head[coord] - tail[coord]) > 1;

const shift = (move) => {
  const [direction, distance] = move.split(' ');
  for (let steps = 0; steps < distance; steps++) {
    switch (direction) {
      case 'R':
        ++head.x;
        if (outOfRange('x')) {
          ++tail.x;
          tail.y = head.y;
        }

        break;
      case 'D':
        --head.y;
        if (outOfRange('y')) {
          --tail.y;
          tail.x = head.x;
        }

        break;
      case 'L':
        --head.x;
        if (outOfRange('x')) {
          --tail.x;
          tail.y = head.y;
        }

        break;
      case 'U':
        ++head.y;
        if (outOfRange('y')) {
          ++tail.y;
          tail.x = head.x;
        }
        
        break;
    };
    
    traversed.push(`${tail.x},${tail.y}`);
  }
};

const input = readInput('input.txt');
// const input = readInput('practice.txt');
solve(input);

// == Initial State ==

// ......
// ......
// ......
// ......
// H.....  (H covers T, s)