const { log, readInput } = require('../../shared/index.js');

solve = (trees) => {
  let maxScenic = 0;
  for (let rowIndex = 1; rowIndex < trees.length - 1; rowIndex++) {
    const row = trees[rowIndex];
    
    for (let treeIndex = 1; treeIndex < row.length - 1; treeIndex++) {
      const tree = row[treeIndex];
      if (!+tree) continue;

      const treesNorth = trees.map(row => row[treeIndex]).slice(0, rowIndex);
      let north = 0;
      treesNorth.reverse().every(treeNorth => {
        ++north;
        return tree > treeNorth;
      });

      const treesEast = [...row.slice(treeIndex + 1)];
      let east = 0;
      treesEast.every(treeEast => {
        ++east;
        return tree > treeEast;
      });

      const treesSouth = trees.map(row => row[treeIndex]).slice(rowIndex + 1);
      let south = 0;
      treesSouth.every(treeSouth => {
        ++south;
        return tree > treeSouth;
      });

      const treesWest = [...row.slice(0, treeIndex)];
      let west = 0;
      treesWest.reverse().every(treeWest => {
        ++west;
        return tree > treeWest;
      });

      maxScenic = Math.max(maxScenic, north * east * south * west);
    };
  };
  log({results: maxScenic});
}

// const input = readInput('practice.txt');
const input = readInput('input.txt');
solve(input);