const { log, readInput } = require('../../shared/index.js');

solve = (trees) => {
  let visibleTrees = (trees.length + trees[0].length) * 2 - 4;

  for (let rowIndex = 1; rowIndex < trees.length - 1; rowIndex++) {
    const row = trees[rowIndex];
    
    for (let treeIndex = 1; treeIndex < row.length - 1; treeIndex++) {
      const tree = row[treeIndex];
      if (!+tree) continue;

      if (trees.map(row => row[treeIndex]).slice(0, rowIndex).every(t => t < tree)) {
        ++visibleTrees;
        continue;
      };

      if ([...row.slice(treeIndex + 1)].every(t => t < tree)) {
        ++visibleTrees;
        continue;
      };

      if (trees.map(row => row[treeIndex]).slice(rowIndex + 1).every(t => t < tree)) {
        ++visibleTrees;
        continue;
      };

      if ([...row.slice(0, treeIndex)].every(t => t < tree)) {
        ++visibleTrees;
        continue;
      };
    };
  };
  log({results: visibleTrees});
}

// const input = readInput('practice.txt');
const input = readInput('input.txt');
solve(input);