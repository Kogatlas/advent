const { log, readInput } = require('../../shared/index.js');

solve = (puzzleInput) => {
  let index = 0;
  let step = new Set(puzzleInput.slice(index, index + 14));

  while(step.size !== 14) {
    step = new Set(puzzleInput.slice(++index, index + 14));
  };
  log({result: index + 14});
}

const input = readInput('input.txt');
// const input = readInput('practice.txt');
solve(input);