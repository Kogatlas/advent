const { log, readInput } = require('../../shared/index.js');

solve = (puzzleInput) => {
  let index = 0;
  let step = new Set(puzzleInput.slice(index, index + 4));

  while(step.size !== 4) {
    step = new Set(puzzleInput.slice(++index, index + 4));
  };
  log({result: index + 4});
}

const input = readInput('input.txt');
// const input = readInput('practice.txt');
solve(input);