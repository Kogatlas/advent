const { log, readInput } = require('../../shared/index.js');

solve = (puzzleInput) => {
  let directories = {};
  let currentDirectory = '/';
  puzzleInput.forEach(line => {
    const [cmd, ...args] = line.split(' ');
    switch (cmd) {
      case '$':
        if (args[0] === 'cd') {
          switch (args[1]) {
            case '..':
              currentDirectory = currentDirectory.replaceAll('/', '/\\').split('\\').filter(e => e);
              currentDirectory.pop();
              currentDirectory = currentDirectory.join('');
              break;
            case '/':
              currentDirectory = '/';
              break;
            default:
              currentDirectory = currentDirectory + args[1] + '/';
          }
        };
        break;
      case 'dir':
        break;
      default:
        let path = currentDirectory.replaceAll('/', '/\\').split('\\').filter(e => e); 
        while (path.length > 0) {
          directories[path.join('')] = (directories[path.join('')] || 0) + +cmd;
          path.pop();
        }
    };
  });
  
  log({results: Object.values(directories).reduce((prev, curr) => {
    if (curr <= 100000) {
      return prev + curr;
    } else {
      return prev;
    }
  }, 0)});
};

const input = readInput('input.txt');
// const input = readInput('practice.txt');
solve(input);