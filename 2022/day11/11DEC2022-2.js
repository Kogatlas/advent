const { log, readInput } = require('../../shared/index.js');
let monkeys = {};
const parseInput = (input) => {
  while (input.length) {
    const [
      name,
      items,
      action,
      test,
      ifTrue,
      ifFalse,
    ] = input.splice(0, 7);

    monkeys[name.replace(':', '')] = {
      items: items.split(':')[1].trim().split(',').map(i => +i),
      action: action.split(' ').slice(-2),
      test: +test.split(' ').slice(-1),
      ifTrue: +ifTrue.split(' ').slice(-1),
      ifFalse: +ifFalse.split(' ').slice(-1),
      inspectedCount: 0
    };
  };
};

solve = (puzzleInput) => {
  parseInput(puzzleInput);
  const lcm = Object.values(monkeys).map(m => m.test).reduce((prev, curr) => prev * curr);
  for (let i = 0; i < 10000; i++) {
    for (let monkey in monkeys) {
      let {items, action, test, ifTrue, ifFalse} = monkeys[monkey];

      items.forEach(old => {
        monkeys[monkey].inspectedCount++;
        old = eval(`old ${action[0]} ${action[1]}`) % lcm;
        if (old % test) throwToMonkey(ifFalse, old)
        else throwToMonkey(ifTrue, old);

      });
      monkeys[monkey].items = [];
    };
  };
  log({
    monkeyBusiness: Object.values(monkeys)
      .map(monkey => monkey.inspectedCount)
      .sort((a, b) => b - a)
      .slice(0, 2)
      .reduce((a, b) => a * b)
  });
};

const throwToMonkey = (target, item) => monkeys[`Monkey ${target}`].items.push(item);

// const input = readInput('practice.txt');
const input = readInput('input.txt');
solve(input);