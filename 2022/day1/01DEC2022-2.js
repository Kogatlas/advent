const { log, readInput } = require('../../shared/index.js');

solve = (puzzleInput) => {
  let totals = new Array(puzzleInput.length).fill(0);

  let index = 0;
  puzzleInput.map((calories) => {
    if (!!calories) {
      totals[index] += +calories;
    } else index++;
  });
    
  log({total: totals.sort((a, b) => b - a).slice(0, 3).reduce((a, b) => a + b)});
}

const input = readInput('input.txt');
solve(input);