const { log, readInput } = require('../../shared/index.js');

solve = (puzzleInput) => {
  let totals = new Array(puzzleInput.length).fill(0);

  let index = 0;
  puzzleInput.map((calories) => {
    if (!!calories) {
      totals[index] += +calories;
    } else index++;
  });
    
  log({max: Math.max(...totals)});
}

const input = readInput('input.txt');
solve(input);