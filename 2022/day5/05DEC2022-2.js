const { log, readInput } = require('../../shared/index.js');

solve = (puzzleInput) => {
  let stacks = [];
  let moves = [];
  puzzleInput.map(line => {
    if (line.includes('move')) {
      moves.push(line.split(' ').filter(i => !isNaN(i)));
    } else if (line.includes('[')) {
      const crates = line.split('');
      for(let index = 0; index < line.length; index+= 4) {
        stacks[index / 4] = stacks[index / 4] || [];
        stacks[index / 4].push(crates.slice(index, index + 3)[1]);
      }
    };
  });

  moves.forEach(move => {
    const amount = move[0];
    const source = stacks[move[1] - 1].filter(crate => !!crate.trim());
    let target = stacks[move[2] - 1].filter(crate => !!crate.trim());
    target = [...source.slice(0, amount), ...target];

    stacks[move[1] - 1] = source.slice(amount);
    stacks[move[2] - 1] = target;
  });
  log({result: stacks.map(stack => stack[0]).join('')});
}

const input = readInput('input.txt');
// const input = readInput('practice.txt');
solve(input);