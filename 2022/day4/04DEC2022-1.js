const { log, readInput, shuffleArray } = require('../../shared/index.js');

solve = (puzzleInput) => {
  let overlap = 0;
  puzzleInput.map(input => {
    const assignments = input.split(',');

    const [a1, a2] = assignments.map(assignment => {
      const [start, end] = assignment.split('-');
      const keys = [...Array(end - start + 1).keys()];
      return keys.map(i => i + +start);
    });

    if (JSON.stringify(a1) === JSON.stringify(a2)) {
      ++overlap;
    } else {
      if (a2.every(section => a1.includes(section))) {
        ++overlap;
      };
      if (a1.every(section => a2.includes(section))) {
        ++overlap;
      };
    }
  });
  log ({overlap});
}

const input = readInput('practice.txt');
// const input = readInput('input.txt');
shuffleArray(input);
solve(input);