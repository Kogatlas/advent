const { log, readInput } = require('../../shared/index.js');

const isSymbol = (number) => !!number && isNaN(number) && number !== '.';

let sum = 0;
solve = (puzzleInput) => {
  puzzleInput.forEach((partNumber, index) => {
    let firstIndex = lastIndex = 0;
    partNumber.split(/(\d+)/).filter(Boolean).forEach((number) => {
      if (!isNaN(number)) {
        firstIndex = lastIndex + partNumber.substring(lastIndex).indexOf(number);
        lastIndex = firstIndex + number.length;

        if (isSymbol(partNumber[firstIndex - 1]) || isSymbol(partNumber[firstIndex + number.length])) {
          sum += +number;
        } else {
          const above = index > 0 
            && puzzleInput[index - 1].substring(firstIndex - 1, firstIndex + number.length + 1);

          const below = !!puzzleInput[index + 1] 
            && puzzleInput[index + 1].substring(firstIndex - 1, firstIndex + number.length + 1);

          if (!!above && above.split('').some(s => isSymbol(s))
            || (!!below && below.split('').some(s => isSymbol(s)))) {
            sum += +number;
          }
        }
      }
    });
  });
}

const input = readInput('input.txt');
solve(input);
log({ sum });