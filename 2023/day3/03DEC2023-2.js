const { log, readInput } = require('../../shared/index.js');

const concatReverse = (line, index) => {
  return line.substring(index - 4, index).split(/(\d+)/).filter(Boolean).at(-1);
}

const concat = (line, index) => {
  return line.substring(index + 1, index + 4).split('').filter(i => !isNaN(i)).join('');
}

let sum = 0;
solve = (input) => {
  input.forEach((line, lineIndex) => {
    line.split('').forEach((char, charIndex) => {
      if (char === "*") {
        let partNumbers = [];

        // Before/left
        if (!isNaN(line[charIndex - 1])) {
          const foundNumber = concatReverse(line, charIndex);
          partNumbers.push(foundNumber);
        }

        // After/right
        if (!isNaN(line[charIndex + 1])) {
          const foundNumber = concat(line, charIndex);
          partNumbers.push(foundNumber);
        }

        const above = lineIndex > 0 && input[lineIndex - 1].substring(charIndex - 1, charIndex + 2);
        const below = !!input[lineIndex + 1] && input[lineIndex + 1].substring(charIndex - 1, charIndex + 2);
        if (!!above) {
          if (/\d\.\d/.test(above)) {
            const left = concatReverse(input[lineIndex - 1], charIndex);
            partNumbers.push(left);

            const right = concat(input[lineIndex - 1], charIndex);
            partNumbers.push(right);
          }
          if (/\d\d\d/.test(above)) {
            partNumbers.push(above);
          }
          if (/\d\d\./.test(above)) {
            const left = concatReverse(input[lineIndex - 1], charIndex + 1);
            partNumbers.push(left);
          }
          if (/\.\d\d/.test(above)) {
            const right = concat(input[lineIndex - 1], charIndex - 1);
            partNumbers.push(right);
          }
          if (/\.\d\./.test(above)) {
            partNumbers.push(above.split('').filter(d => +d > 0).join());
          }
          if (/\.\.\d/.test(above)) {
            const right = concat(input[lineIndex - 1], charIndex);
            partNumbers.push(right);
          }
          if (/\d\.\./.test(above)) {
            const left = concatReverse(input[lineIndex - 1], charIndex);
            partNumbers.push(left);
          }
          if (/\.\.\./.test(above)) {
          }
        }
        if (!!below) {
          if (/\d\.\d/.test(below)) {
            const left = concatReverse(input[lineIndex + 1], charIndex);
            partNumbers.push(left);

            const right = concat(input[lineIndex + 1], charIndex);
            partNumbers.push(right);
          }
          if (/\d\d\d/.test(below)) {
            partNumbers.push(below);
          }
          if (/\d\d\./.test(below)) {
            const left = concatReverse(input[lineIndex + 1], charIndex + 1);
            partNumbers.push(left);
          }
          if (/\.\d\d/.test(below)) {
            const right = concat(input[lineIndex + 1], charIndex - 1);
            partNumbers.push(right);
          }
          if (/\.\d\./.test(below)) {
            partNumbers.push(below.split('').filter(d => +d > 0).join());
          }
          if (/\.\.\d/.test(below)) {
            const right = concat(input[lineIndex + 1], charIndex);
            partNumbers.push(right);
          }
          if (/\d\.\./.test(below)) {
            const left = concatReverse(input[lineIndex + 1], charIndex);
            partNumbers.push(left);
          }
          if (/\.\.\./.test(below)) {
          }
        }

        if (partNumbers.length === 2) sum += (+partNumbers[0] * +partNumbers[1]);
      }
    });
  });
}

const input = readInput('input.txt');
solve(input);
log({ sum });