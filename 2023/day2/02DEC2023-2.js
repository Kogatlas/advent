const { log, readInput, shuffleArray, varToString } = require('../../shared/index.js');

let sum = 0;
solve = (puzzleInput) => {
  puzzleInput.forEach(game => {
    const [id, sets] = game.split(':');

    let colorMax = {
      'blue': Number.NEGATIVE_INFINITY,
      'green': Number.NEGATIVE_INFINITY,
      'red': Number.NEGATIVE_INFINITY,
    };

    sets.split(';').forEach(set => {
      set.split(',').forEach(sample => {
        const [count, color] = sample.trim().split(' ');
        colorMax[color] = +colorMax[color] < count ? count : colorMax[color];
      });
    });

    const power = (Object.values(colorMax).reduce((prev, curr) => prev * curr));
    sum += power;
  });
}

const input = readInput('input.txt');
shuffleArray(input);
solve(input);
log({ sum });