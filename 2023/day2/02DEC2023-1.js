const { log, readInput, shuffleArray, varToString } = require('../../shared/index.js');

const colorLimits = {
  blue: 14,
  green: 13,
  red: 12,
};

let sum = 0;
solve = (puzzleInput) => {
  log({ puzzleInput });
  puzzleInput.forEach(game => {
    log({ game });
    const [id, sets] = game.split(':');
    log({ id });
    log({ sets });
    const invalid = sets.split(';').some(set => {
      return set.split(',').some(item => {
        const [count, color] = item.trim().split(' ');
        return count > colorLimits[color];
      });
    });
    sum += invalid ? 0 : +id.replaceAll(/\D/g, '');
  });
}

const input = readInput('input.txt');
shuffleArray(input);
solve(input);
log({sum});