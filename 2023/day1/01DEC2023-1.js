const { log, readInput, shuffleArray, varToString } = require('../../shared/index.js');

let sum = 0;
solve = (puzzleInput) => {
    puzzleInput.forEach(row => {
        const number = row.replaceAll(/\D/g, '');
        sum += +`${number.slice(0, 1)}${number.slice(-1)}`;
    });
}

const input = readInput('input.txt');
shuffleArray(input);
solve(input);
log({sum});