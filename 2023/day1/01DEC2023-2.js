const { log, readInput, shuffleArray, varToString } = require('../../shared/index.js');

parseNumbers = (input) => {
    const numbers = {
        1: 'one',
        2: 'two',
        3: 'three',
        4: 'four',
        5: 'five',
        6: 'six',
        7: 'seven',
        8: 'eight',
        9: 'nine',
    };
    Object.entries(numbers).forEach(([key, value]) => {
        // log({value});
        // log({key});
        input = input.replaceAll(value, value.slice(0,1) + key + value.slice(1));
    });
    // log({input});
    return input;
}

let sum = 0;
solve = (puzzleInput) => {
    // log({puzzleInput});
    puzzleInput.forEach(row => {
        // log({row});
        const number = parseNumbers(row).replaceAll(/\D/g, '');
        // log({number});
        sum += +`${number.slice(0, 1)}${number.slice(-1)}`;
    });
}

const input = readInput('input.txt');
shuffleArray(input);
solve(input);
log({sum});