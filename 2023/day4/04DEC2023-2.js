const { log, readInput } = require('../../shared/index.js');

let cardCount = {
  0: 0,
}
solve = (input) => {
  input.forEach((card, index) => {
    cardCount[index] = !!cardCount[index] ? ++cardCount[index] : 1;
    const numbers = card.split(':')[1];

    const [winningNumbers, testNumbers] = numbers.split('|');
    let matches = 0;
    winningNumbers.split(' ').filter(Boolean).forEach((number) => {
      if (testNumbers.split(' ').filter(Boolean).includes(number)) {
        ++matches;
        cardCount[index + matches] = cardCount[index + matches]
          ? cardCount[index + matches] + 1 * cardCount[index]
          : 1 * cardCount[index];
      }
    });
  })
}

const input = readInput('input.txt');
solve(input);
log({ total: Object.values(cardCount).reduce((p, c) => c + p) });