const { log, readInput, shuffleArray, varToString } = require('../../shared/index.js');

let points = 0;
solve = (input) => {
  input.forEach(card => {
    const numbers = card.split(':')[1];
    const [winningNumbers, testNumbers] = numbers.split('|');
    let score;
    winningNumbers.split(' ').filter(Boolean).forEach(number => {
      if (testNumbers.split(' ').filter(Boolean).includes(number)) {
        score = !!score ? score * 2 : 1;
      }
    });
    points += !!score ? score : 0;
  })
}

const input = readInput('input.txt');
shuffleArray(input);
solve(input);
log({ points });