const log = require('./log');
const readInput = require('./readInput');
const shuffleArray = require('./shuffleArray');
const varToString = require('./varToString');

module.exports = {
  log, readInput, shuffleArray, varToString,
}