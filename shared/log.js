module.exports = log = (...values) => {
  if (Object.keys(values).length > 1) {
    let msg = '';
    values.forEach(value => {
      const name = varToString(value);
      if (msg.length > 0) msg += ' ';
      msg += name.toUpperCase() + ':';
      msg += value[name];
    });
    console.log(msg);
  } else {
    const name = varToString(values[Object.keys(values)[0]]);
    console.log(name.toUpperCase() + ':', values[0][name]);
  }
};