const fs = require('fs');

module.exports = readInput = (file) => {
  var input = fs.readFileSync(file, 'utf-8').split('\n');
  return input.length > 1 ? input : input[0];
};